<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_a', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('class');
            $table->string('time_create');
            $table->string('time_update');
            $table->softDeletes();
        });

        Schema::create('data_b', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('a_id');
            $table->foreign('a_id')->references('id')->on('data_a');
            $table->string('name');
            $table->string('time_create');
            $table->string('time_update');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_a');
        Schema::dropIfExists('data_b');
    }
}
