<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use App\Models\DataA;
use App\Models\DataB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function getAll()
    {
        try {
            $data = DataB::with('dataClass')->get();

            $response =
                [
                    'message' => 'Get Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getAllDataA()
    {
        try {
            $data = DataA::all();

            $response =
                [
                    'message' => 'Get Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getAllDataB()
    {
        try {
            $data = DataB::all();

            $response =
                [
                    'message' => 'Get Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function postDataA(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'class' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {

            $data = DataA::create([
                'id' => Str::uuid()->toString(),
                'class' => $request->class,
                'time_create' => date('Y-m-d H:i:s'),
                'time_update' => date('Y-m-d H:i:s')
            ]);

            $response =
                [
                    'message' => 'Tambah Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 201);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function postDataB(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'a_id' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {

            $cek = DataA::where('id', $request->a_id)->first();

            if (!$cek) {

                $response =
                    [
                        'message' => 'Data A Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            $data = DataB::create([
                'id' => Str::uuid()->toString(),
                'a_id' => $request->a_id,
                'name' => $request->name,
                'time_create' => date('Y-m-d H:i:s'),
                'time_update' => date('Y-m-d H:i:s')
            ]);

            $response =
                [
                    'message' => 'Tambah Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 201);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function putDataA(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'class' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {

            $cek = DataA::where('id', $request->id)->first();

            if (!$cek) {

                $response =
                    [
                        'message' => 'ID Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            DataA::where('id', $request->id)->update([
                'class' => $request->class,
                'time_update' => date('Y-m-d H:i:s')
            ]);

            $data = DataA::where('id', $request->id)->first();

            $response =
                [
                    'message' => 'Update Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function putDataB(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'a_id' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        try {

            $cek1 = DataB::where('id', $request->id)->first();

            $cek2 = DataA::where('id', $request->a_id)->first();

            if (!$cek1) {

                $response =
                    [
                        'message' => 'ID Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            if (!$cek2) {

                $response =
                    [
                        'message' => 'Data A Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            DataB::where('id', $request->id)->update([
                'a_id' => $request->a_id,
                'name' => $request->name,
                'time_update' => date('Y-m-d H:i:s')
            ]);

            $data = DataB::with('dataClass')->where('id', $request->id)->first();

            $response =
                [
                    'message' => 'Update Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getDataAById($id)
    {
        try {

            $data = DataA::where('id', $id)->first();

            if (!$data) {

                $response =
                    [
                        'message' => 'Data Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            $response =
                [
                    'message' => 'Get Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function getDataBById($id)
    {
        try {

            $data = DataB::with('dataClass')->where('id', $id)->first();

            if (!$data) {

                $response =
                    [
                        'message' => 'Data Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            $response =
                [
                    'message' => 'Get Data Berhasil',
                    'data' => $data,
                ];
            return response()->json($response, 200);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function deleteDataB($id)
    {
        try {

            $data = DataB::where('id', $id)->first();

            if (!$data) {

                $response =
                    [
                        'message' => 'Data Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            DataB::where('id', $id)->delete();

            $response =
                [
                    'message' => 'Delete Data Berhasil'
                ];
            return response()->json($response, 200);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function deleteDataA($id)
    {
        try {

            $data = DataA::where('id', $id)->first();

            if (!$data) {

                $response =
                    [
                        'message' => 'Data Tidak Ditemukan',
                        'data' => null,
                    ];
                return response()->json($response, 404);

            }

            $cek_id = DataB::where('a_id', $id)->first();

            if ($cek_id) {

                $response =
                    [
                        'message' => 'Data Tidak Boleh Dihapus',
                        'data' => null,
                    ];

                return response()->json($response, 422);

            }

            DataA::where('id', $id)->delete();

            $response =
                [
                    'message' => 'Delete Data Berhasil'
                ];
            return response()->json($response, 200);

        } catch (QueryException $e) {

            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}