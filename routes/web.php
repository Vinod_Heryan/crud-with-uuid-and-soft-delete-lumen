<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });

$router->post('/api/v1/post/data/a','Controller@postDataA');

$router->post('/api/v1/post/data/b','Controller@postDataB');

$router->get('/api/v1/data/a','Controller@getAllDataA');

$router->get('/api/v1/data/b','Controller@getAllDataB');

$router->get('/api/v1/data','Controller@getAll');

$router->post('/api/v1/put/data/a','Controller@putDataA');

$router->post('/api/v1/put/data/b','Controller@putDataB');

$router->get('/api/v1/data/a/{id}','Controller@getDataAById');

$router->get('/api/v1/data/b/{id}','Controller@getDataBById');

$router->delete('/api/v1/delete/data/a/{id}','Controller@deleteDataA');

$router->delete('/api/v1/delete/data/b/{id}','Controller@deleteDataB');



